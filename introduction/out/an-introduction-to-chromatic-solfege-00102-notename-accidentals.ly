\version "2.18.2"
\include "aaron.ly"
\include "chromatic-template.ly"
\include "lilypond-book-preamble.ly"
\language "aaron"

	\makescore 
  \relative do' {
	do di dai dao dai di do s do de daw dae daw de do  \bar "|" \break  re ri rai rao rai ri re s re ra raw rae raw ra re  \bar "|" \break  mi ma mai mao mai ma mi s mi me maw mae maw me mi  \bar "|" \break  fa fi fai fao fai fi fa s fa fe faw fae faw fe fa  \bar "|" \break  sol si sai sao sai si sol s sol se saw sae saw se sol  \bar "|" \break  la li lai lao lai li la s la le law lae law le la  \bar "|" \break  ti ta tai tao tai ta ti s ti te taw tae taw te ti  \bar "|" \break  do di dai dao dai di do s do de daw dae daw de do  \bar "|" \break 
  }
  #'(( festival-tempo . 150 )( festival-voice . voice_us2_mbrola )( do-compile . #t )( do-play . #f ))

\paper  {
	#(set! paper-alist (cons '("a4insides" . (cons (* 8 in) (* 3 in))) paper-alist))
	#(set-paper-size "a4insides")
}


\version "2.18.2"
\include "aaron.ly"
\include "chromatic-template.ly"
\include "lilypond-book-preamble.ly"
\language "aaron"

	\makescore 
  \relative do' {
	\absolute { do' raw'' s ra' mae'' s re' maw'' s me' faw'' s mi' fe'' s fa' saw'' s se' lae'' s sol' law'' s le' tae'' s la' taw'' s te' daw''' s ti' de''' s do'' }
  }
  #'(( festival-tempo . 170 )( festival-voice . voice_us2_mbrola )( do-compile . #t )( do-play . #f )( mark-irregular-accidentals . #t ))

\paper  {
	#(set! paper-alist (cons '("a4insides" . (cons (* 8 in) (* 3 in))) paper-alist))
	#(set-paper-size "a4insides")
}


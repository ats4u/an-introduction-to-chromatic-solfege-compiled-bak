\version "2.18.2"
\include "aaron.ly"
\include "chromatic-template.ly"
\include "lilypond-book-preamble.ly"
\language "aaron"

	\makescore 
  \relative do' {
	\absolute { do' sai' s ra' li' s re' lai' s me' ta' s mi' tai' s fa' dai'' s se' ri'' s sol' rai'' s le' ma'' s la' mai'' s te' fai'' s ti' fao'' s do'' }
  }
  #'(( festival-tempo . 170 )( festival-voice . voice_us2_mbrola )( do-compile . #t )( do-play . #f )( mark-irregular-accidentals . #t ))

\paper  {
	#(set! paper-alist (cons '("a4insides" . (cons (* 8 in) (* 3 in))) paper-alist))
	#(set-paper-size "a4insides")
}


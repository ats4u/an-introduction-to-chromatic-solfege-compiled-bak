\version "2.18.2"
\include "aaron.ly"
\include "chromatic-template.ly"
\include "lilypond-book-preamble.ly"
\language "aaron"

	\makescore 
  \relative do' {
	do me se taw s te sol mi ra s re fa le de s  \bar "|" \break  do la fi ri s mi sol te ra s re ti si ma s  \bar "|" \break  fi li di mi s fa re ti si s la do me se s  \bar "|" \break  sol mi di li s ti re fa le s la fi ri ta
  }
  #'(( festival-tempo . 150 )( festival-voice . voice_us2_mbrola )( do-compile . #t )( do-play . #f ))

\paper  {
	#(set! paper-alist (cons '("a4insides" . (cons (* 8 in) (* 3 in))) paper-alist))
	#(set-paper-size "a4insides")
}


\version "2.18.2"
\include "aaron.ly"
\include "chromatic-template.ly"
\include "lilypond-book-preamble.ly"
\language "aaron"

	\makescore 
  \relative do' {
	\absolute { do' re' s di' ri' s re' mi' s ri' ma' s mi' fi' s fa' sol' s fi' si' s sol' la' s si' li' s la' ti' s li' ta' s ti' di'' s do'' }
  }
  #'(( festival-tempo . 170 )( festival-voice . voice_us2_mbrola )( do-compile . #t )( do-play . #f )( mark-irregular-accidentals . #t ))

\paper  {
	#(set! paper-alist (cons '("a4insides" . (cons (* 8 in) (* 3 in))) paper-alist))
	#(set-paper-size "a4insides")
}


\version "2.18.2"
\include "aaron.ly"
\include "chromatic-template.ly"
\include "lilypond-book-preamble.ly"
\language "aaron"

	\makescore 
  \relative do' {
	\absolute { do' daw'' s ra' rae'' s re' raw'' s me' mae'' s mi' maw'' s fa' faw'' s se' sae'' s sol' saw'' s le' lae'' s la' law'' s te' tae'' s ti' taw'' s do'' }
  }
  #'(( festival-tempo . 170 )( festival-voice . voice_us2_mbrola )( do-compile . #t )( do-play . #f )( mark-irregular-accidentals . #t ))

\paper  {
	#(set! paper-alist (cons '("a4insides" . (cons (* 8 in) (* 3 in))) paper-alist))
	#(set-paper-size "a4insides")
}


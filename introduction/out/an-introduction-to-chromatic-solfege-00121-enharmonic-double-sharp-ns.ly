\version "2.18.2"
\include "aaron.ly"
\include "chromatic-template.ly"
\include "lilypond-book-preamble.ly"
\language "aaron"

	\makescore 
  \relative do' {
	re dai s mi rai s fi mai s sol fai s la sai s ti lai s di tai s re dai  \bar "|" \break  re dai s di tai s ti lai s la sai s sol fai s fi mai s mi rai s re dai
  }
  #'(( festival-tempo . 150 )( festival-voice . voice_us2_mbrola )( do-compile . #t )( do-play . #f ))

\paper  {
	#(set! paper-alist (cons '("a4insides" . (cons (* 8 in) (* 3 in))) paper-alist))
	#(set-paper-size "a4insides")
}


\version "2.18.2"
\include "aaron.ly"
\include "chromatic-template.ly"
\include "lilypond-book-preamble.ly"
\language "aaron"

	\makescore 
  \relative do' {
	ra4 fa le do
  }
  #'(( festival-tempo . 150 )( festival-voice . voice_us2_mbrola )( do-compile . #t )( do-play . #f )( show-scale-diagram . #t )( always-show-zero-fret . #t )( fret-range . (1 . 12) )( fret-positions . ( 5 4 4 3 ) ))

\paper  {
	#(set! paper-alist (cons '("a4insides" . (cons (* 3 in) (* 3 in))) paper-alist))
	#(set-paper-size "a4insides")
}


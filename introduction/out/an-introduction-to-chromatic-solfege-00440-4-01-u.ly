\version "2.18.2"
\include "aaron.ly"
\include "chromatic-template.ly"
\include "lilypond-book-preamble.ly"
\language "aaron"

	\makescore 
  \relative do' {
	\absolute { dao'4 sao'4 s rao'4 lao'4 s mao'4 tao'4 s fao'4 dao''4 s sao'4 rao''4 s lao'4 mao''4 s tao'4 fao''4 s dao''2 }
  }
  #'(( festival-tempo . 170 )( festival-voice . voice_us2_mbrola )( do-compile . #t )( do-play . #f ))

\paper  {
	#(set! paper-alist (cons '("a4insides" . (cons (* 8 in) (* 3 in))) paper-alist))
	#(set-paper-size "a4insides")
}


\version "2.18.2"
\include "aaron.ly"
\include "chromatic-template.ly"
\include "lilypond-book-preamble.ly"
\language "aaron"

	\makescore 
  \relative do' {
	do me se taw s di, mi sol te s re, fa le de s  \bar "|" \break  ri, fi la do s mi, sol te ra s fa, le de maw s  \bar "|" \break  fi, la do me s sol, te ra fe s si, ti re fa s  \bar "|" \break  la, do me se s li, di mi sol s ti, re fa le s  \bar "|" \break  do, me se taw s s
  }
  #'(( festival-tempo . 150 )( festival-voice . voice_us2_mbrola )( do-compile . #t )( do-play . #f ))

\paper  {
	#(set! paper-alist (cons '("a4insides" . (cons (* 8 in) (* 3 in))) paper-alist))
	#(set-paper-size "a4insides")
}


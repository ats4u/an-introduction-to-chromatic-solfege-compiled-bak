\version "2.18.2"
\include "aaron.ly"
\include "chromatic-template.ly"
\include "lilypond-book-preamble.ly"
\language "aaron"

	\makescore 
  \relative do' {
	\absolute { do' daw'' s di' de'' s re' raw'' s ri' ra'' s mi' maw'' s fa' faw'' s fi' fe'' s sol' saw'' s si' se'' s la' law'' s li' le'' s ti' taw'' s do'' }
  }
  #'(( festival-tempo . 170 )( festival-voice . voice_us2_mbrola )( do-compile . #t )( do-play . #f )( mark-irregular-accidentals . #t ))

\paper  {
	#(set! paper-alist (cons '("a4insides" . (cons (* 8 in) (* 3 in))) paper-alist))
	#(set-paper-size "a4insides")
}


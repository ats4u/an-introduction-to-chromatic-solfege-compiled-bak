\version "2.18.2"
\include "aaron.ly"
\include "chromatic-template.ly"
\include "lilypond-book-preamble.ly"
\language "aaron"

	\makescore 
  \relative do' {
	\absolute { do'4 di'4 s di'4 re'4 s re'4 ri'4 s ri'4 mi'4 s mi'4 fa'4 s fa'4 fi'4 s fi'4 sol'4 s sol'4 si'4 s si'4 la'4 s la'4 li'4 s li'4 ti'4 s ti'4 do''4 s do''2 }
  }
  #'(( festival-tempo . 200 )( festival-voice . voice_us2_mbrola )( do-compile . #t )( do-play . #f ))

\paper  {
	#(set! paper-alist (cons '("a4insides" . (cons (* 8 in) (* 3 in))) paper-alist))
	#(set-paper-size "a4insides")
}


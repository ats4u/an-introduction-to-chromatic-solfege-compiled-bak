\version "2.18.2"
\include "aaron.ly"
\include "chromatic-template.ly"
\include "lilypond-book-preamble.ly"
\language "aaron"

	\makescore 
  \relative do' {
	\absolute { dai'4 lai'4 s rai'4 tai'4 s mai'4 dai''4 s fai'4 rai''4 s sai'4 mai''4 s lai'4 fai''4 s tai'4 sai''4 s dai''2 }
  }
  #'(( festival-tempo . 170 )( festival-voice . voice_us2_mbrola )( do-compile . #t )( do-play . #f ))

\paper  {
	#(set! paper-alist (cons '("a4insides" . (cons (* 8 in) (* 3 in))) paper-alist))
	#(set-paper-size "a4insides")
}

